<?php
require 'steamauth/steamauth.php';
?>

<!DOCTYPE html>
<html lang="ru">
<head>
    <title>DotaStats</title>
    <meta charset="UTF-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="robots" content="index, follow">
    <meta name="google" content="notranslate">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="">
    <style>
        <?php include './src/assets/scss/style.scss'; ?>
    </style>
</head>
<body>
    <div class="background">
        <video width=100% loop="loop" autoplay="">
            <source src="./src/assets/images/backgroubnd.mp4">
            <source src="./src/assets/images/backgroubnd.webm">
        </video>
    </div>
    <header class="header">
        <div class="header__navigation">
            <nav class="nav">
                <ul class ="nav__list">
                    <li class="nav__item">
                        <a class="nav__link" href="index.php">Main</a>
                    </li>
                    <li class="nav__item">
                        <a class="nav__link" href="about.php">About</a>
                    </li>
                    <li class="nav__item">
                        <a class="nav__link" href="https://www.dota2.com/patches/">Updates</a>
                    </li>
                    <?php 
                        if(isset($_SESSION['steamid'])){
                            echo '<div class="header__navigation">
                                        <nav class="nav">
                                            <ul class ="nav__list">
                                                <li class="nav__item">
                                                    <a class="nav__link" href="stats.php">Stats</a>
                                                </li>';
                        }
                    ?>
                </ul>
            </nav>
        </div>
        <div class="header__login">  
                <ul class="nav__log">
                    <ul class = "nav__login">
                        <?php
                            if(!isset($_SESSION['steamid'])) {
                            loginbutton(); //login button
                            }  
                            else {
                                include ('steamauth/userInfo.php'); //To access the $steamprofile array
                                //Protected content
                                echo $steamprofile['personaname'];
                                $a = $steamprofile['avatarmedium'];
                                echo "<img src=$a>";
                                logoutbutton(); //Logout Button
                            }     
                        ?>
                    </ul>
                </ul>
        </div>
    </header>
    <div class="block_about">
    <div class="post">
        <div class="post_content">
            <p class="post__title">
                Курсовая работа по теме: Клиент-серверное приложение.
            </p>
            <img class="post_preview" src="./src/assets/images/dota_label.jpg">
            <p class="post_text">
                Полное наименование системы: «Веб-приложение для отслеживания рейтинга».
                <br>
                Заказчик: Томский государственный университет систем управления и
                радиоэлектроники, кафедра комплексной информационной безопасности
                электронно-вычислительных систем (КИБЭВС).
                <br>
                Исполнил: Керноз Игорь Сергеевич 718-1.
            </p>
        </div>
    </div>
</div>
</body>
</html>
